public class Dog{
	public int age;
	public String name;
	public boolean mixed;
	

	public void babyBreed(){
		if (age <=2){
			System.out.println( name + " is still not ready to have kids");
		}
		else{
			if(mixed){
				System.out.println(name + " can possibly share more than one breed type with it's child");
			}
			else{
				System.out.println( name + " will only share one breed type with it's child");
			}
		}
		
	}
	
	public void breedMixedCheck(){
		if (mixed != true){
			System.out.println(name + " can only give birth to one breed");
		}
		else{
			System.out.println(name + " can give birth to multiple breeds");
		}
		
	}
}